#include "ros/ros.h"
#include "sensor_msgs/Range.h"
#include <sstream>


sensor_msgs::Range my_range;


void chatterCallback(sensor_msgs::Range range_distance)
{
	my_range.range = range_distance.range/2;
}

int main (int argc, char **argv){

	ros::init(argc, argv, "ultrasonic_listener");
	ros::NodeHandle n;

	ros::Subscriber sub = n.subscribe("ultrasound_raw", 1000, chatterCallback);
	
        ros::Publisher publisher = n.advertise<sensor_msgs::Range>("ultrasonic_filtered",1000);
	ros::Rate loop_rate(10);
	
	
	sensor_msgs::Range range_distance;
	range_distance.header.frame_id = "ultrasound";
	my_range.header.frame_id = "ultrasound";
	
	
	range_distance.field_of_view = 0.785;
	range_distance.min_range = 0.02;
	range_distance.max_range = 4.00;
	
	
	my_range.field_of_view = 0.785;
	my_range.min_range = 0.02;
	my_range.max_range = 4.00;


	while (ros::ok()){
		//range_distance.range = ((float)rand()/RAND_MAX)*4;
		//range_distance.range = range_distance;
		//sensor.range = Distance;
		//ROS_INFO("%.2f", sensor.range);
		ROS_INFO("%f", my_range.range);
		publisher.publish(my_range);
		//publisher.publish(range_distance);
		ros::spinOnce();
		loop_rate.sleep();
}

	return 0;
}
