#include "ros/ros.h"
#include "sensor_msgs/Range.h"


using namespace sensor_msgs;


void chatterCallback(const sensor_msgs::Range::ConstPtr& range_distance)
{
	//ROS_INFO("Range: [%f]", msg->data.c_str());
	ROS_INFO("Range: %f", range_distance->range);
}

int main (int argc, char **argv)
{
	ros::init(argc, argv, "ultrasonic_listener");
	ros::NodeHandle n;
	ros::Subscriber sub = n.subscribe("ultrasound_range", 1000, chatterCallback);

	ros::spin();

	return 0;
}
